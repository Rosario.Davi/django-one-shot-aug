from django.urls import path
from todos.views import (
    TodolistListView,
    TodolistDetailView,
    TodolistCreateView,
    TodolistUpdateView,
    TodolistDeleteView,
    TodoitemCreateView,
    TodoitemUpdateView,
)

urlpatterns = [
    path("", TodolistListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodolistDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodolistCreateView.as_view(), name="todo_list_create"),
    path(
        "<int:pk>/edit/", TodolistUpdateView.as_view(), name="todo_list_update"
    ),
    path(
        "<int:pk>/delete/",
        TodolistDeleteView.as_view(),
        name="todo_list_delete",
    ),
    path(
        "items/create/", TodoitemCreateView.as_view(), name="todo_item_create"
    ),
    path(
        "items/<int:pk>/edit/",
        TodoitemUpdateView.as_view(),
        name="todo_item_update",
    ),
]
