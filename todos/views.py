from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from todos.models import TodoList, TodoItem
from django.urls import reverse, reverse_lazy


class TodolistListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "do_list"


class TodolistDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "tasks"


class TodolistCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse("todo_list_detail", args=[self.object.pk])


class TodolistUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse("todo_list_detail", args=[self.object.pk])


class TodolistDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoitemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/itemcreate.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list_list")


class TodoitemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todos/itemedit.html"
    success_url = reverse_lazy("todo_list_detail")
